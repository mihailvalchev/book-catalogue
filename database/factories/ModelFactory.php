<?php
use App\User;
use App\Author;
use App\Book;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/**
 * factory for applicants
 */
$factory->define(App\Author::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName . ' ' . $faker->lastName,
        'note' => $faker->realText($maxNbChars = rand(20,65)),
    ];
});

/**
 * factory for jobs
 */
$factory->define(App\Book::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->realText($maxNbChars = rand(15,32)),
        'purchase_year' => rand(1950,2017),
        'note' => $faker->realText($maxNbChars = rand(20,65)),
        'cover' => '/uploads/thebookthief_3.jpg',
        'author_id' => rand(1,25),
    ];
});
