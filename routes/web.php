<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/test', function () {
    return view('test');
});

Route::group(['middleware' => 'auth'], function () {

    //Logged in
	
	//Books
	Route::get('/', 'BooksController@index')->name('books');
	Route::get('/books', 'BooksController@index')->name('books');
	Route::get('/books/editor/{id?}', 'BooksController@editor');
	Route::post('/books/create', 'BooksController@create');
	Route::post('/books/editor/{id}', 'BooksController@edit');
	Route::get('/books/delete/{id}', 'BooksController@delete');

	//Books Search	
	Route::get('/search/{title}', 'BooksController@search');
	
	//Authors
	Route::get('/authors', 'AuthorsController@index')->name('authors');
	Route::get('/authors/editor/{id?}', 'AuthorsController@editor');
	Route::post('/authors/create', 'AuthorsController@create');
	Route::post('/authors/editor/{id}', 'AuthorsController@edit');
	Route::get('/authors/delete/{id}', 'AuthorsController@delete');

	//User Permissions	
	Route::get('/users/editor', 'UserPermissionsController@editor');
	Route::post('/users/editor', 'UserPermissionsController@update_permissions');
	Route::get('/not_allowed', function () {
		return view('not_allowed');
	});
});