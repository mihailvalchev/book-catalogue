<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = ['name', 'note'];
	
    /**
     *  Book type relation
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
	public function books()
	{
	  return $this->hasMany('App\Book');
	}
}
