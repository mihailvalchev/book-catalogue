<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title', 'author_id', 'purchase_year', 'cover', 'note'];

    /**
     *  Author type relation
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function author()
    {
        return $this->belongsTo('App\Author');
    }
}
