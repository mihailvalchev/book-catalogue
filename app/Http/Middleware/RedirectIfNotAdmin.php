<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
		
        if (Auth::guard($guard)->check() && Auth::user()->role == 2 ) {
            return redirect('/not_allowed');
        }
		
        if (!Auth::guard($guard)->check() && $request->path() !== 'login' && $request->path() !== 'register') {
            return redirect('/login');
        }

        return $next($request);
    }
}
