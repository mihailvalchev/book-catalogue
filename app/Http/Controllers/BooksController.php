<?php

namespace App\Http\Controllers;

use Request;
use App\Book;
use App\Author;
use App\Http\Requests\CreateBookRequest;
use App\Http\Requests\EditBookRequest;

class BooksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('adminauth');
    }

    /**
     * Show the Books page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$books = Book::orderBy('purchase_year', 'DESC')->with('author')->paginate(5);
		$authors = Author::orderBy('created_at', 'DESC')->get();
		
        return view('books', compact(['books', 'sort', 'authors']));
    }

    /**
     * Show the Books Editor page
     *
     * @return \Illuminate\Http\Response
     */
    public function editor($id = NULL)
    {
		$books = Book::orderBy('created_at', 'DESC')->with('author')->get();
		$authors = Author::orderBy('created_at', 'DESC')->get();
		
		if($id){
			$book = Book::where('id', $id)->with('author')->first();
		}
        return view('editor.books', compact(['book', 'books', 'authors']));
    }

    /**
     * Create Book
     *
     */
    public function create(CreateBookRequest $request)
    {
		$postData = $request->all();
		unset($postData['_token']);
		
        $file = $request->cover;
        $image_name = time()."-".$file->getClientOriginalName();
        $file->move('uploads', $image_name);
		$postData['cover'] = '/uploads/' . $image_name;		
		
		Book::create($postData);
        return redirect()->back()->with(['success' => '"' . $postData['title'] . '" was created.']);
    }

    /**
     * Update Book
     *
     */
    public function edit(EditBookRequest $request, $id)
    {
		$postData = $request->all();
		unset($postData['_token']);
		if( isset($postData['cover']) ){
			$file = $request->cover;
			$image_name = time()."-".$file->getClientOriginalName();
			$file->move('uploads', $image_name);
			$postData['cover'] = $image_name;		
		}
		
		Book::find($id)->update($postData);
		
        return redirect()->back()->with(['success' => '"' . $postData['title'].'" was updated.']);
    }

    /**
     * Delete Book
     *
     */
    public function delete($id)
    {
		$book = Book::find($id);
		$book_title = $book->title;
		$book->delete();
        return redirect()->back()->with(['success' => '"' . $book_title.'" was deleted.']);
		
    }

    /**
     * Search Books
     *
     */
    public function search($title)
    {
		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'DESC';
		$books = Book::where('title', 'LIKE', '%' . $title . '%')->orderBy('purchase_year', $sort)->with('author')->paginate(5);
		
        return view('search', compact(['books', 'title', 'sort']));
		
    }
	
}
