<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;
use App\Http\Requests\CreateAuthorRequest;
use App\Http\Requests\EditAuthorRequest;

class AuthorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('adminauth');
    }

    /**
     * Show the authors page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$authors = Author::orderBy('created_at', 'DESC')->with('books')->paginate(5);
        return view('authors', compact(['authors']));
    }

    /**
     * Show the Authors Editor page
     *
     * @return \Illuminate\Http\Response
     */
    public function editor($id = NULL)
    {
		
		$authors = Author::with('books')->orderBy('created_at', 'DESC')->get();
		if($id){
			$author = Author::where('id', $id)->with('books')->first();
		}
        return view('editor.authors', compact(['authors', 'author']));
    }

    /**
     * Create Author
     *
     */
    public function create(CreateAuthorRequest $request)
    {
		$postData = $request->all();
		unset($postData['_token']);
		
		Author::create($postData);
        return redirect()->back()->with(['success' => '"' . $postData['name'] . '" was created.']);
    }

    /**
     * Update Author
     *
     */
    public function edit(EditAuthorRequest $request, $id)
    {
		$postData = $request->all();
		unset($postData['_token']);
		
		$author = Author::find($id);
		$author_name = $author->name;
		$author->update($postData);
		
        return redirect()->back()->with(['success' => $author_name . ' was updated.']);
    }

    /**
     * Delete Author
     *
     */
    public function delete($id)
    {
		$author = Author::find($id);
		$author_name = $author->name;
		$author->delete();
        return redirect()->back()->with(['success' => $author_name . ' was deleted.']);
		
    }
}
