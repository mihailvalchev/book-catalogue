<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Carbon\Carbon;

class UserPermissionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('adminauth');
    }

    /**
     * Users editor
     *
     */
    public function editor()
    {		
		$users = User::get();
        return view('editor.users', compact('users'));
    }

    /**
     * User Permissions Update
     *
     */
    public function update_permissions(Request $request)
    {		
		$postData = $request->all();
		unset($postData['_token']);
		
		foreach($postData['user'] as $id => $role){
			$user = User::find($id);
			$user->role = $role;
			$user->update();	
		}
		
        return redirect()->back()->with(['success' => count($postData['user']) . ' users were updated.']);
    }

}
