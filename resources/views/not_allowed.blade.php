@extends('layouts.app')

@section('head')
	<title>{{ config('app.name', 'Laravel') }} - Nothing to see here</title>
@endsection

@section('content')
    <h1>Nothing to see here</h1>
	<span>You don't have permissions to view the contents of this page...</span>
	
@endsection
