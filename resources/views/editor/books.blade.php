@extends('layouts.app')

@section('head')
	<title>{{ config('app.name', 'Laravel') }} - Books Editor</title>
@endsection

@section('content')
    <h1>Books Editor</h1>
	<hr>
	@if(count($authors)>0)
	<form action="{{isset($book)?'' : '/books/create'}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
		<div class="row">
			<div class="col-xs-12">
				<h3>{{isset($book)?'Edit' : 'Create New'}} Book</h3>
			</div>
		</div>
		@if(count($errors)>0)
		<div class="row">
			<div class="col-xs-12">			
				@if ( count( $errors ) > 0 )
					<div class="alert alert-danger">
					<b>Error.</b><br>
					@foreach ($errors->all() as $error)
							{!! $error !!}<br>
					@endforeach
					</div>
				@endif
			</div>
		</div>
		@endif
		<div class="row">
			<div class="col-xs-12 col-sm-2">
				@if(isset($book))
				<input type="hidden" name="id" value="{{$book->id}}">
				@endif
				<input type="text" name="title" value="{{isset($book)?$book->title : old('title')}}" placeholder="Book Title" class="form-control">
			</div>
			<div class="col-xs-12 col-sm-2">
				<select name="author_id" id="" class="form-control">
				@foreach($authors as $author)
					<option value="{{$author->id}}" @if(isset($book) && $author->id==$book->author->id) selected @endif>{{$author->name}}</option>
				@endforeach
				</select>
			</div>
			<div class="col-xs-12 col-sm-2">
				<input type="number" name="purchase_year" value="{{isset($book)?$book->purchase_year : old('purchase_year')}}" placeholder="Year" class="form-control">
			</div>
			<div class="col-xs-12 col-sm-2">
				<input type="file" name="cover" value="" placeholder="Cover" class="form-control">
			</div>
			<div class="col-xs-12 col-sm-2">
				<input type="text" name="note" value="{{isset($book)?$book->note : old('note')}}" placeholder="Note" class="form-control">
			</div>
			<div class="col-xs-12 col-sm-2">
				<input id="submitBtn" type="submit" value="{{isset($book)?'Update' : 'Create'}} Book" class="btn btn-success form-control">
			</div>
		</div>
	</form>
	@endif
	<div class="row">
		<div class="col-xs-12">
			<h3>Books List</h3>
		</div>
	</div>
	@if(count(Session::has('success'))>0)
	<div class="row">
		<div class="col-xs-12">
			@if ( Session::has('success') )
                <div class="alert alert-success">
				<b>Success.</b><br>
					{{ Session::get('success')}}
                </div>
			@endif
		</div>
	</div>
	@endif
	@if(count($books)>0)
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Title</th>
					<th>Author</th>
					<th>Purchase Year</th>
					<th>Cover</th>
					<th>Note</th>
					<th></th>
					<th>Created</th>
				</tr>
			</thead>
			<tbody>
			@foreach($books as $book)
				<tr>
					<td style="vertical-align: middle"><b>{{$book->title}}</b></td>
					<td style="vertical-align: middle">{{$book->author->name}}</td>
					<td style="vertical-align: middle">{{$book->purchase_year}}</td>
					<td style="vertical-align: middle;text-align:center;"><img src="{{$book->cover}}" alt="" style="max-width:80px;max-height:80px;"></td>
					<td style="vertical-align: middle">{{$book->note}}</td>
					<td style="vertical-align: middle">
						<a href="/books/editor/{{$book->id}}">Edit</a>
						<a href="/books/delete/{{$book->id}}">Delete</a>
					</td>
					<td style="vertical-align: middle">{{$book->created_at}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	@elseif(count($books)==0 && count($authors)==0)
		<span>
			Start by creating an author <a href="/authors/editor">Here</a>
		</span>
	
	
	@elseif(count($books)==0)
		<span>
			You don't have any books yet.
		</span>
	@endif
@endsection
