@extends('layouts.app')

@section('head')
	<title>{{ config('app.name', 'Laravel') }} - Authors Editor</title>
@endsection

@section('content')
    <h1>Authors Editor</h1>
	<hr>
	<form action="{{isset($author)?'' : '/authors/create'}}" method="POST">
        {{ csrf_field() }}
		<div class="row">
			<div class="col-xs-12">
				<h3>{{isset($author)?'Edit' : 'Create New'}} Author</h3>
			</div>
		</div>
		@if(count($errors)>0)
		<div class="row">
			<div class="col-xs-12">			
				@if ( count( $errors ) > 0 )
					<div class="alert alert-danger">
					<b>Error.</b><br>
					@foreach ($errors->all() as $error)
							{!! $error !!}<br>
					@endforeach
					</div>
				@endif
			</div>
		</div>
		@endif
		<div class="row">
			<div class="col-xs-12 col-sm-5">
				@if(isset($author))
				<input type="hidden" name="id" value="{{$author->id}}">
				@endif
				<input type="text" name="name" value="{{isset($author)?$author->name : old('name')}}" placeholder="Author Name" class="form-control">
			</div>
			<div class="col-xs-12 col-sm-5">
				<input type="text" name="note" value="{{isset($author)?$author->note : old('note')}}" placeholder="Notes" class="form-control">
			</div>
			<div class="col-xs-12 col-sm-2">
				<input id="submitBtn" type="submit" value="{{isset($author)?'Update' : 'Create'}} Author" class="btn btn-success form-control">
			</div>
		</div>
	</form>
	<div class="row">
		<div class="col-xs-12">
			<h3>Authors List</h3>
		</div>
	</div>
	@if(count(Session::has('success'))>0)
	<div class="row">
		<div class="col-xs-12">
			@if ( Session::has('success') )
                <div class="alert alert-success">
				<b>Success.</b><br>
					{{ Session::get('success')}}
                </div>
			@endif
		</div>
	</div>
	@endif
	@if(count($authors)>0)
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Name</th>
					<th>Note</th>
					<th>Books</th>
					<th></th>
					<th>Created</th>
				</tr>
			</thead>
			<tbody>
			@foreach($authors as $author)
				<tr>
					<td>{{$author->name}}</td>
					<td>{{$author->note}}</td>
					<td>{{$author->books->count()}}</td>
					<td>
						<a href="/authors/editor/{{$author->id}}">Edit</a>
						<a href="/authors/delete/{{$author->id}}">Delete</a>
					</td>
					<td>{{$author->created_at}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	@else
		<span>There are no authors yet.</span>
	@endif
@endsection
