@extends('layouts.app')

@section('head')
	<title>{{ config('app.name', 'Laravel') }} - Users Editor</title>
@endsection

@section('content')
    <h1>Users Editor</h1>
	<hr>
	@if(count(Session::has('success'))>0)
	<div class="row">
		<div class="col-xs-12">
			@if ( Session::has('success') )
                <div class="alert alert-success">
				<b>Success.</b><br>
					{{ Session::get('success')}}
                </div>
			@endif
		</div>
	</div>
	@endif
	@if(count($users)>0)
		<form action="" method="POST">
        {{ csrf_field() }}
		<table class="table table-striped">
			<thead>
				<tr>
					<th>User Name</th>
					<th>Permissions</th>
					<th>Created At</th>
				</tr>
			</thead>
			<tbody>
			@foreach($users as $user)
				<tr>
					<td style="vertical-align: middle"><b>{{$user->name}}</b></td>
					<td style="vertical-align: middle">
						@if($user->role!==0)
						<select name="user[{{$user->id}}]" id="" class="form-control">
							<option value="1" @if($user->role==1) selected @endif>Admin</option>
							<option value="2" @if($user->role==2) selected @endif>Regular User</option>
						</select>
						@else
							<b>Admin</b>
						@endif
					</td>
					<td style="vertical-align: middle">{{$user->created_at}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		@if(count($users)==1)
			<span>You are the only user.</span>
		@else
			<input type="submit" class="btn btn-success pull-right" value="Update">
		@endif
		</form>
	@endif
@endsection
