@extends('layouts.app')

@section('head')
	<title>Test - Mihail Valchev</title>
@endsection

@section('content')
<div class="page">

<!-- Part  1 -->
<h3>1. Implement calculation for N!, N! = 1*2*...*N. (5! = 1*2*3*4*5 = 120)</h3>
<?php 

// Non Recursive
function non_recursive_factorial ($x){
	$y = 1;
	
	for( $i=1;$i<=$x; $i++){
		$y *= $i;
	}
	return $y;
}

echo 'Non recursive: '.non_recursive_factorial(5).'<br>';


// Recursive
function recursive_factorial ($x){
	
	//base
	if($x <= 1) {
		return 1;
	}
	
	return $x * recursive_factorial( $x - 1 );
}

echo 'Recursive: '.recursive_factorial(5);
?>

<h5>Code</h5>
Non-recursive
<pre>
function non_recursive_factorial ($x){
	$y = 1;
	
	for( $i=1;$i<=$x; $i++){
		$y *= $i;
	}
	return $y;
}

echo 'Non recursive: '.non_recursive_factorial(5);
</pre>
Recursive
<pre>
// Recursive
function recursive_factorial ($x){
	
	if($x <= 1) {
		return 1;
	}
	
	return $x * recursive_factorial( $x - 1 );
}

echo 'Recursive: '.recursive_factorial(5);
</pre>
<hr>

<!-- Part  2 -->
<h3>2. Sort the array of integer values ascending. Example {10, 5, 20, 3, 100, -20}. The purpose of the task is to implement an algorithm and not use the built in sort function.</h3>
<?php
function custom_sort_asc($array) {
	
	for( $j = 0; $j < count($array); $j ++ ) {
		for( $i = 0; $i < count($array)-1; $i ++ ){

			if( $array[$i] > $array[$i+1] ) {
				
				$temp = $array[$i+1];
				$array[$i+1]=$array[$i];
				$array[$i]=$temp;
			}       
		}
	}
	return $array;
}

$array = [10, 5, 20, -20, 3, 1000];
?>
Unsorted:
<pre><?php print_r($array); ?></pre>
Sorted: 
<pre><?php print_r(custom_sort_asc($array)); ?></pre>
<h5>Code:</h5>
<pre>
function custom_sort_asc($array) {
	
	for( $j = 0; $j < count($array); $j ++ ) {
		for( $i = 0; $i < count($array)-1; $i ++ ){

			if( $array[$i] > $array[$i+1] ) {
				
				$temp = $array[$i+1];
				$array[$i+1]=$array[$i];
				$array[$i]=$temp;
			}       
		}
	}
	return $array;
}

$array = [10, 5, 20, -20, 3, 1000];
print_r(custom_sort_asc($array));
</pre>
<hr>



<!-- Part  3 -->
<h3>3. You have created a button that triggers an AJAX event. How can you make sure it will never trigger it twice at the same time?</h3>
<p><b>Solution:</b> Disable it on click and enable later in the AJAX promise</p>

<div class="demo">

	<!-- Loader -->
	<div id="loader">Demo</div>
	<!-- Button -->
	<button type="button" id="ajaxCallBtn" onclick="ajaxCall()">Make Ajax Call</button>
	<script type="text/javascript">
		function ajaxCall(){
			
			var btn =  document.getElementById('ajaxCallBtn');
			var loader =  document.getElementById('loader');
			
			//Disable the button
			btn.setAttribute('disabled', true);
			//Notify that it is loading
			loader.innerHTML = "Loading...";
			
			//Ajax Request
			setTimeout(function(){
				
				//Enable the button in the ajax promise
				btn.removeAttribute('disabled');
				loader.innerHTML = "Done";
			}, 2000);
		}
	</script>
</div>



<h5>Code</h5>
<pre>
function ajaxCall(){
	
	var btn =  document.getElementById('ajaxCallBtn');
	var loader =  document.getElementById('loader');
	
	//Disable the button
	btn.setAttribute('disabled', true);
	//Notify that it is loading
	loader.innerHTML = "Loading...";
	
	//Ajax Request
	setTimeout(function(){
		
		//Enable the button in the ajax promise
		btn.removeAttribute('disabled');
		loader.innerHTML = "Done";
	}, 2000);
}
</pre>
<hr>





<!-- Part  4 -->
<h3>4. You have the following 2 tables with their values:</h3>
<div style="display:block; width: 100%;text-align:left;">
<table style="display:inline-block;" border="1">
	<thead>
		<th colspan="2">Table 'a'</th>
	</thead>
	<thead>
		<th>id</th>
		<th>name</th>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>x</td>
		</tr>
		<tr>
			<td>2</td>
			<td>y</td>
		</tr>
		<tr>
			<td>3</td>
			<td>z</td>
		</tr>
	</tbody>
</table>
<table style="display:inline-block;" border="1">
	<thead>
		<th colspan="2">Table 'b'</th>
	</thead>
	<thead>
		<th>id</th>
		<th>name</th>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>q</td>
		</tr>
		<tr>
			<td>2</td>
			<td>w</td>
		</tr>
		<tr>
			<td>6</td>
			<td>e</td>
		</tr>
	</tbody>
</table>
</div>

<br><br>
a. What will the following command return:
<code>SELECT * FROM
a INNER JOIN b ON a.id = b.id</code><br>

<table border="1">
	<thead>
		<th colspan="4">Result</th>
	</thead>
	<thead>
		<th>a.id</th>
		<th>a.name</th>
		<th>b.id</th>
		<th>b.name</th>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>x</td>
			<td>1</td>
			<td>q</td>
		</tr>
		<tr>
			<td>2</td>
			<td>y</td>
			<td>2</td>
			<td>w</td>
		</tr>
	</tbody>
</table>


<br><br>
b. What will the following command return:
<code>SELECT * FROM
a LEFT JOIN b ON a.id = b.id</code><br>

<table border="1">
	<thead>
		<th colspan="4">Result</th>
	</thead>
	<thead>
		<th>a.id</th>
		<th>a.name</th>
		<th>b.id</th>
		<th>b.name</th>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>x</td>
			<td>1</td>
			<td>q</td>
		</tr>
		<tr>
			<td>2</td>
			<td>y</td>
			<td>2</td>
			<td>w</td>
		</tr>
		<tr>
			<td>3</td>
			<td>z</td>
			<td>NULL</td>
			<td>NULL</td>
		</tr>
	</tbody>
</table>
<br><br>

c. What will the following command return:
<code>SELECT * FROM
a LEFT JOIN b ON a.id = b.id
WHERE b.name = 'P'</code><br>
<table border="1">
	<thead>
		<th colspan="4">Result</th>
	</thead>
	<thead>
		<th>a.id</th>
		<th>a.name</th>
		<th>b.id</th>
		<th>b.name</th>
	</thead>
</table>

<br><br>
d. What will the following command return:
<code>SELECT * FROM
a LEFT JOIN b ON a.id = b.id AND b.name = 'P'</code><br>

<table border="1">
	<thead>
		<th colspan="4">Result</th>
	</thead>
	<thead>
		<th>a.id</th>
		<th>a.name</th>
		<th>b.id</th>
		<th>b.name</th>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>x</td>
			<td>NULL</td>
			<td>NULL</td>
		</tr>
		<tr>
			<td>2</td>
			<td>y</td>
			<td>NULL</td>
			<td>NULL</td>
		</tr>
		<tr>
			<td>3</td>
			<td>z</td>
			<td>NULL</td>
			<td>NULL</td>
		</tr>
	</tbody>
</table>
</div>

<style>
body {	
	font: 13px Arial, Helvetica, sans-serif !important;
	color: #000;
}
.page {
	max-width: 860px;
	margin: auto;
	padding: 60px 0px;
}
pre {
	border:1px solid lightgrey;
	background: whitesmoke;
	border-radius: 3px;
	padding: 15px;
}
th, td {
	padding: 5px;
}
</style>
@endsection