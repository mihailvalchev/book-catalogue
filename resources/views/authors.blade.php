@extends('layouts.app')

@section('head')
	<title>{{ config('app.name', 'Laravel') }} - My Authors</title>
@endsection

@section('content')
    <h1>My Authors</h1>
	<hr>
	@if(count($authors)>0)
		@foreach($authors as $author)
			<h4>{{$author->name}} ({{$author->books->count()}} Books)</h4>
		@endforeach
		{{$authors->links()}}
	@elseif(count($authors)==0)
		<span>
			You don't have any authors yet. Start adding <a href="/authors/editor">Here</a>
		</span>
	@endif
@endsection
