<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
							<li class="{{ Request::is('/') ? 'active' : '' }}">
								<a href="{{ url('/') }}">My Books</a>
							</li>
							<li class="{{ Request::is('/authors ') ? 'active' : '' }}">
								<a href="{{ url('/authors') }}">My Authors</a>
							</li>
							@if(Auth::user()->role<2)
							<li class="{{ Request::is('/books/editor') ? 'active' : '' }}">
								<a href="{{ url('/books/editor') }}">Book Editor</a>
							</li>
							<li class="{{ Request::is('/author/editor') ? 'active' : '' }}">
								<a href="{{ url('/authors/editor') }}">Author Editor</a>
							</li>
							<li class="{{ Request::is('/users/editor') ? 'active' : '' }}">
								<a href="{{ url('/users/editor') }}">User Permissions</a>
							</li>
							@endif
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endguest
                    <li><a href="{{ url('/test') }}">Test</a></li>
            </ul>
        </div>
    </div>
</nav>