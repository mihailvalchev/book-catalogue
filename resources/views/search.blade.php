@extends('layouts.app')

@section('head')
	<title>{{ config('app.name', 'Laravel') }} - My Books</title>
@endsection

@section('content')
    <h1>Search Results for "{{$title}}"</h1>
	<span>In order of purchasing year</span>
	<hr>
	@if(count($books)>0)
		<div class="row">
			<?php $i=0; ?>
			@foreach($books as $book)
				@if($i%6==0)</div><div class="row">@endif
				<div class="col-xs-6 col-md-2 text-center">
						<img src="{{$book->cover}}" alt="{{$book->title}}" class="img-responsive">
						<h3>{{$book->title}}</h3>
						<h5>{{$book->author->name}} ({{$book->author->books->count()}})</h5>
						<span>Purchased: {{$book->purchase_year}}</span><hr>
				</div>
				<?php $i++; ?>
			@endforeach
		</div>
		{{ $books->links() }}
	@else
		<span>
			Could not find any books with title like "{{$title}}"
		</span>
	@endif
@endsection
